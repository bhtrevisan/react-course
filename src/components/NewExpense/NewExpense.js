import React, {useState} from "react";

import "./NewExpense.css";
import ExpenseForm from "./ExpenseForm";

const NewExpense = (props) => {
  // Function to receive data from ExpenseForm and pass data to App.js
  const saveSubmittedForm = (enteredExpenseData) => {
    const expenseData = {
      ...enteredExpenseData,
      id: Math.random().toString(),
    };
    console.log("ExpenseData on NEW EXPENSE", expenseData);
    // Calls function from App.js to pass data
    props.onAddExpense(expenseData);
    setNewExpenseShow(false);
  };

  const [newExpenseShow, setNewExpenseShow] = useState(false);

  const cancelForm = () => {
    setNewExpenseShow(false);
  }

  if (newExpenseShow == false) {
    return (
      <div className="new-expense">
        <button onClick={() => setNewExpenseShow(true)} >Add New Expense</button>
      </div>
    );
  }

  return (
    <div className="new-expense">
      <ExpenseForm onSubmittedForm={saveSubmittedForm} onCancel={cancelForm}></ExpenseForm>
    </div>
  );
};

export default NewExpense;
