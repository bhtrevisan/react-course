import "./ExpensesList.css";
import ExpenseItem from "./ExpenseItem";

const ExpensesList = (props) => {
  if (props.items.length === 0)
    return <h2 className="expenses-list__fallback">No expenses found!</h2>;

  return (
    <ul className="expenses-list">
      {props.items.map((elem) => (
        <ExpenseItem key={elem.id} expense={elem} />
      ))}
    </ul>
  );
};

export default ExpensesList;
