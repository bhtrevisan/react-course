import React, { useState } from "react";

import "./ExpenseItem.css";
import Card from "../UI/Card";
import ExpenseDate from "./ExpenseDate";

const ExpenseItem = (props) => {
  return (
    <li>
      <Card className="expense-item">
        <div>
          <ExpenseDate props={props.expense.date}></ExpenseDate>
        </div>
        <div className="expense-item__description">
          <h2>{props.expense.title}</h2>
          <div className="expense-item__price">R${props.expense.amount}</div>
        </div>
      </Card>
    </li>
  );
};

export default ExpenseItem;
