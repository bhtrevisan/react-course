import React, { useState } from "react";

import "./Expenses.css";
import ExpenseItem from "./ExpenseItem";
import Card from "../UI/Card";
import ExpenseFilter from "../ExpenseFilter/ExpenseFilter";
import ExpensesList from "./ExpensesList";
import ExpensesChart from "../Chart/ExpensesChart";

const Expenses = (props) => {
  // Variable to store the year filter value
  const [selectFilterYear, setSelectFilterYear] = useState("2021");

  const selectFilterChange = (selectData) => {
    setSelectFilterYear(selectData);
  };

  // Variable to store values based on filter value
  const expensesFiltered = props.items.filter((elem) => {
    return elem.date.getFullYear() == selectFilterYear;
  });

  return (
    <div>
      <Card className="expenses">
        <ExpenseFilter
          onFilterChange={selectFilterChange}
          selected={selectFilterYear}
        ></ExpenseFilter>
        <ExpensesChart expenses={expensesFiltered} />
        <ExpensesList items={expensesFiltered}></ExpensesList>
      </Card>
    </div>
  );
};

export default Expenses;
