## Expense Tracker

## 📜 Description
It started on a React course I took and its goal is to go through React best practices.
It is an application that can be used to organize expenses and it shows the amount spent by month in a desired year.

## 🖼️ Screenshots
I'll add some prints to show how this project looks.

### Main Screen
![MainScreen](./public/MainScreen.png)

### Add Expense Screen
![AddExpenseScreen](./public/AddExpenseScreen.png)

## 💻 Requirements
You will need:
* npm;
* react;

## 🔨 Installation 
To install this project in your local machine, follow this steps:

Create a folder in your local machine.
Then run

`git clone {this project link}`

Go to project folder.

`npm install`


## ☕ Using Project
To use this project, you need to start this application on your localhost.

Go to project folder, which has te package.json and run:

`npm start`

## 🔧 Fixes and new features
This project is under development and the next steps are:
- Login option;
- Save your added expenses;
- Not allow empty expenses;
- ...

## 🙂 Author
[@bhtrevisan](https://gitlab.com/bhtrevisan)
